
//Playing The Cards Lab
//Jess Sullivan

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;
enum Rank {Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, King, Queen, Jack, Ace, Joker};
enum Suit {Spades, Clubs, Hearts, Diamonds};
struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card Card)
{
	cout << Card.Rank << " of ";
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank >= card2.Rank)
	{
		return card1;
	}
	else
	{
		return card2;
	}
}

int main()
{
	//Card1
	Card card1;
	card1.Rank = Seven;
	card1.Suit = Spades;
	PrintCard(card1);

	switch (card1.Suit)
	{
	case Spades:
		cout << "Spades \n";
		break;
	case Clubs:
		cout << "Clubs \n";
		break;
	case Hearts:
		cout << "Hearts \n";
		break;
	case Diamonds:
		cout << "Diamonds \n";
		break;
	}

	//Card2
	Card card2;
	card2.Rank = Queen;
	card2.Suit = Hearts;
	PrintCard(card2);

	switch (card2.Suit)
	{
	case Spades:
		cout << "Spades \n";
		break;
	case Clubs:
		cout << "Clubs \n";
		break;
	case Hearts:
		cout << "Hearts \n";
		break;
	case Diamonds:
		cout << "Diamonds \n";
		break;
	}

	HighCard(card1, card2);

	(void)_getch();
	return 0;
}
